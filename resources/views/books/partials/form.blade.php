
{{-- This form is used for Creating and Editing books --}}



{{--Book Title--}}
<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">

    <label for="name" class="col-md-4 control-label">Title</label>

    <div class="col-md-6">
        {!! Form::text('title', null, array('class' => 'form-control', 'placeholder'=> 'Enter title here..')) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>


{{--Book Author--}}
<div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">

    <label for="client" class="col-md-4 control-label">Book Author</label>

    <div class="col-md-6">
        {!! Form::text('author', null, array('class' => 'form-control', 'placeholder'=> 'Enter author name here..')) !!}

        @if ($errors->has('author'))
            <span class="help-block">
                <strong>{{ $errors->first('author') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Book Year--}}
<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">

    <label for="year" class="col-md-4 control-label">Year</label>

    <div class="col-md-6">
        {!! Form::text('year', null, array('class' => 'form-control', 'placeholder'=> 'Enter year here..')) !!}
        <!--{!! Form::text('year', null, array('class' => 'form-control', 'placeholder'=> 'Enter year here..')) !!}-->

        @if ($errors->has('year'))
            <span class="help-block">
                <strong>{{ $errors->first('year') }}</strong>
            </span>
        @endif
    </div>
</div>


{{--Book Language--}}
<div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">

    <label for="link" class="col-md-4 control-label">Language</label>

    <div class="col-md-6">
        {!! Form::text('language', null, array('class' => 'form-control', 'placeholder'=> 'Enter book language here..')) !!}

        @if ($errors->has('language'))
            <span class="help-block">
                <strong>{{ $errors->first('language') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Book Orginal Language--}}
<div class="form-group{{ $errors->has('original_language') ? ' has-error' : '' }}">

    <label for="description" class="col-md-4 control-label">Original Language</label>

    <div class="col-md-6">
        {!! Form::text('original_language', null, array('class' => 'form-control', 'placeholder'=> 'Enter original book language here..')) !!}

        @if ($errors->has('original_language'))
            <span class="help-block">
                <strong>{{ $errors->first('original_language') }}</strong>
            </span>
        @endif
    </div>
</div>

<br>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-shield"></i> Submit
        </button>
    </div>
</div>
