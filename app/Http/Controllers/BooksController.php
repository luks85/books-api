<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Books;
use Illuminate\Support\Facades\Auth;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Books::all();

        if($request['year'] && $request['author']) {
            $books = Books::where([
                ['year', $request['year']], 
                ['author', $request['author']]])->paginate(10);
        } else if ($request['author']) {
            $books = Books::where('author', $request['author'])->paginate(10);
        } else if ($request['year']) {
            $books = Books::where('year', $request['year'])->paginate(10);
        } else {
            //all
            $books = Books::paginate(10);;
        }

        return $books->toJson();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( ! Auth::user())
        {
            dd('there was problem saying you are not logged in');
            return;
        }

        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ( ! Auth::user()) {
            return 'error : You are not logged in!';
        }
        $inss = $request->all();
        $book = Books::create($inss);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return specific book
        $book = Books::find($id);

        if ($book)
            return $book->toJson();
        else
            return 'error: book not found';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if ( ! Auth::user()) {
            return 'error : You are not logged in!';
        }
        $inss = $request->all();
        $book = Books::find($id);
        $book = $book->update($inss);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if ( ! Auth::user()) {
            return 'error : You are not logged in!';
        }

        $books = Books::find($id);
        $books->delete();
    }
}
